import sys
import matplotlib.pyplot as plt

import nltk
import numpy
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from tokenFun import getFile, limatizeText, clear_board

numpy.set_printoptions(threshold=sys.maxsize)
listOfBooks = {
    'dziady-dziady-widowisko-czesc-i': 'https://wolnelektury.pl/media/book/txt/dziady-dziady-widowisko-czesc-i.txt'
    , 'pan-tadeusz': 'https://wolnelektury.pl/media/book/txt/pan-tadeusz.txt'
    # , 'konrad-wallenrod': 'https://wolnelektury.pl/media/book/txt/konrad-wallenrod.txt'
    # , 'reduta-ordona': 'https://wolnelektury.pl/media/book/txt/reduta-ordona.txt'
    # , 'oda-do-mlodosci': 'https://wolnelektury.pl/media/book/txt/oda-do-mlodosci.txt'
    # , 'krzyzacy-tom-pierwszy': 'https://wolnelektury.pl/media/book/txt/krzyzacy-tom-pierwszy.txt'
    # , 'krzyzacy-tom-drugi': 'https://wolnelektury.pl/media/book/txt/krzyzacy-tom-drugi.txt'
    # , 'potop-tom-pierwszy': 'https://wolnelektury.pl/media/book/txt/potop-tom-pierwszy.txt'
    # , 'potop-tom-drugi': 'https://wolnelektury.pl/media/book/txt/potop-tom-drugi.txt'
    # , 'potop-tom-trzeci': 'https://wolnelektury.pl/media/book/txt/potop-tom-trzeci.txt'
    # , 'quo-vadis': 'https://wolnelektury.pl/media/book/txt/quo-vadis.txt'
    # , 'pan-wolodyjowski': 'https://wolnelektury.pl/media/book/txt/pan-wolodyjowski.txt'
    # , 'ogniem-i-mieczem-tom-pierwszy': 'https://wolnelektury.pl/media/book/txt/ogniem-i-mieczem-tom-pierwszy.txt'
                                       ,'ogniem-i-mieczem-tom-drugi': 'https://wolnelektury.pl/media/book/txt/ogniem-i-mieczem-tom-drugi.txt'
}
nltk.download('punkt')
nltk.download('stopwords')
corpus = []
labels = []
for target in listOfBooks:
    labels.append(target)
    print("Pobranie ksiazki: " + target)
    book = getFile(listOfBooks[target]);
    corpus.append(book)
vectorizer = CountVectorizer(stop_words=stopwords.words('polish'), token_pattern=r'\b[^\d\W]+\b')

corpusCount = vectorizer.fit_transform(corpus)
counts = pd.DataFrame(corpusCount.toarray(),
                      columns=vectorizer.get_feature_names())
cos1 = cosine_similarity(corpusCount)
clear_board(np.matrix(cos1),labels)


tfidfVectorizer = TfidfVectorizer()
x = tfidfVectorizer.fit_transform(corpus)

sortDctForZip = dict(sorted(tfidfVectorizer.vocabulary_.items(), key=lambda item: item[1], reverse=True))
zipDct = {}
fs = []
rang = 1
for dct in sortDctForZip:
    zipDct.update({dct: sortDctForZip.get(dct) * rang})
    fs.append(sortDctForZip.get(dct) * rang)
    rang = rang + 1
print(zipDct)
plt.clf()
# plt.xscale('log')
# plt.yscale('log')
plt.title('Zipf  plot')
plt.xlabel('rank')
plt.ylabel('frequency')
plt.plot(range(0, len(fs[:50])), fs[:50])
plt.show()
