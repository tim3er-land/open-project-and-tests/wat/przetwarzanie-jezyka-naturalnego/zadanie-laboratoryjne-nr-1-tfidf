import urllib
import string
import nltk
import pandas as pd
from stempel import StempelStemmer
from nltk.corpus import stopwords


def listToString(s):
    str1 = ""
    for ele in s:
        if ele is not None:
            str1 += ' ' + ele
    return str1


def getFile(target_url):
    file = urllib.request.urlopen(target_url)
    txt = ''
    for line in file:
        decoded_line = line.decode("utf-8")
        txt = txt + decoded_line
    return txt


def limatizeText(text):
    stemmer = StempelStemmer.default()
    tokens = nltk.word_tokenize(text)
    lemToken = set()
    for token in tokens:
        lemToken.add(stemmer.stem(token))
    return listToString(lemToken)


def prepareToken(target_url):
    regular_punct = list(string.punctuation)
    stemmer = StempelStemmer.default()

    file = urllib.request.urlopen(target_url)
    txt = getFile(target_url)
    tokens = nltk.word_tokenize(txt)
    # print(tokens)
    # print(tokens.__len__())

    tokensWithoutSW = [token for token in tokens if token not in stopwords.words('polish')]
    # print(tokensWithoutSW)
    # print(tokensWithoutSW.__len__())

    tokensWithoutSWAndPunctation = [token for token in tokensWithoutSW if token not in regular_punct]
    # print(tokensWithoutSWAndPunctation)
    # print(tokensWithoutSWAndPunctation.__len__())

    lemToken = set()
    for token in tokensWithoutSWAndPunctation:
        lemToken.add(stemmer.stem(i).__str__().lower())
        # print(lemToken)
    return lemToken


def clear_board(x,labels):
    board = pd.DataFrame(x, columns=labels, index=labels)
    print(board)